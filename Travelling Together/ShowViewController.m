//
//  ShowViewController.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/26.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "ShowViewController.h"
#import "TYCollectionViewLayout.h"
#import "SDRotationLoopProgressView.h"
#import "NetworkSingleton.h"
#import "ShowCollectionViewCell.h"
#import "MJExtension.h"
#import "ShowModel.h"
#import "GBADetailViewController.h"
#import "MJRefresh.h"

#define URL @"http://chanyouji.com/api/destinations.json?page=1"
#define KWidth_Scale    [UIScreen mainScreen].bounds.size.width/300.0f
#define RGBA(r, g, b, a)    [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

static NSString *cellIdentifier = @"ShowViewController";
static const CGFloat MJDuration = 2.0;

@interface ShowViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TYFlowDelegate>

@property (nonatomic,strong)UISegmentedControl *segmentedControl;
@property (nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataArray;
@property (nonatomic,strong)TYCollectionViewLayout *flowLayout;
@property (nonatomic,strong)SDRotationLoopProgressView *LoadView;
@property (nonatomic,strong)ShowViewController *showVC;
@property (nonatomic,strong)UIView *firstView;
@property (nonatomic,strong)UIView *secondView;

@end

@implementation ShowViewController
- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    NSArray * segmenteArray = [[NSArray alloc] initWithObjects:@"国外", @"国内", nil];
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:segmenteArray];
    self.segmentedControl.frame = CGRectMake(5, 69, self.view.frame.size.width - 10, 30);
    self.segmentedControl.selectedSegmentIndex = 2;
    self.segmentedControl.tintColor = [UIColor colorWithRed:16/255.0 green:135/255.0 blue:217/255.0 alpha:1.0];
    
    [self.segmentedControl addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventValueChanged];
    
    
    [self.view addSubview:self.segmentedControl];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 150) style:(UITableViewStyleGrouped)];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    
    
    //    self.navigationItem.title = @"旅行攻略";
    
    //set NavigationBar 背景颜色&title 颜色
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:16/255.0 green:135/255.0 blue:217/255.0 alpha:1.0]];
    
    CGRect rect = CGRectMake(0, 0, 188, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"旅行攻略";
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"System Italic" size:18.0];
    self.navigationItem.titleView = label;
    
    
    _firstView =[[UIView alloc] initWithFrame:CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 150)];
    //    _firstView.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:_firstView];
    
    _secondView =[[UIView alloc] initWithFrame:CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 150)];
    //    _secondView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:_secondView];
    [self firstSegment];
    [self initCollectionView];
    
    __unsafe_unretained __typeof(self) weakSelf = self;
    
    // 下拉刷新
    self.collectionView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
            
            // 结束刷新
            [weakSelf.collectionView.mj_header endRefreshing];
        });
    }];
    [self.collectionView.mj_header beginRefreshing];
    
    
}

-(void)initCollectionView
{
    //初始化自定义layout
    self.flowLayout = [[TYCollectionViewLayout alloc] init];
    self.flowLayout.degelate = self;
    _flowLayout.colCount = 2;
    _collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout: _flowLayout];
    //注册显示的cell的类型
    
    UINib *cellNib=[UINib nibWithNibName:@"ShowCollectionViewCell" bundle:nil];
    [_collectionView registerNib:cellNib forCellWithReuseIdentifier:cellIdentifier];
    
    _collectionView.frame = CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 170);
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = RGBA(200, 200, 200, 0.25);
    [self.view addSubview:_collectionView];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //重用cell
    ShowCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell setContentWith:_dataArray[indexPath.item]];
    
    ShowModel *s = self.dataArray[indexPath.row];
    
    cell.p_Label.text = [NSString stringWithFormat:@"旅行地 %@",s.poi_count];
    cell.p_Label.textColor = [UIColor whiteColor];
    cell.p_Label.textAlignment = NSTextAlignmentCenter;
    NSLog(@"s.poi_count=%@",s.poi_count);
    
    return cell;
}


- (void)shareAction:(UISegmentedControl *)sender
{
    NSInteger index = [self.segmentedControl selectedSegmentIndex];
    switch (index) {
        case 0:
            [self firstSegment];
            break;
        case 1:
            [self secondSegment];
            break;
        default:
            break;
    }
}


- (void)firstSegment
{
    _firstView.frame  = CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 150);
    _secondView.frame = CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 150);
    
    _firstView.hidden  = NO;
    _secondView.hidden = YES;
    [self.dataArray removeAllObjects];
    [self loadData];
    [_tableView reloadData];
}

- (void)secondSegment
{
    
    _firstView.frame  = CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 150);
    _secondView.frame = CGRectMake(5, 105, self.view.frame.size.width - 10, self.view.frame.size.height - 150);
    
    _firstView.hidden  = YES;
    _secondView.hidden = NO;
    [self.dataArray removeAllObjects];
    [self loadData2];
    [_tableView reloadData];
}

-(void)loadData
{
    
    [self showLoadView];
    
    [[NetworkSingleton sharedManager] getResultWithParameter:nil url:URL successBlock:^(id responseBody) {
        
        
        
        NSArray *arr=responseBody[0][@"destinations"];
//        NSLog(@"arr=%@",arr);
        for (int i=0; i<arr.count; i++) {
            ShowModel *ss=[[ShowModel alloc]init];
            [ss setValuesForKeysWithDictionary:arr[i]];
            [self.dataArray addObject:ss];
//            NSLog(@"poi_count=%@",ss.poi_count);
//            NSLog(@"show_id=%@",ss.show_id);
//            NSLog(@"ss.image_url=%@",ss.image_url);
        }//for
        
        //
        //        NSArray *arr=responseBody[0][@"destinations"];
        //        NSLog(@"arr=%@",arr);
        //
        //       //通过字典数组，制作一个模型数组。
        //       _dataArray = [ShowModel objectArrayWithKeyValuesArray:[responseBody[0] objectForKey:@"destinations"]];
        
        
        NSArray *arr2=responseBody[1][@"destinations"];
//        NSLog(@"arr2=%@",arr2);
        for (int i=0; i<arr2.count; i++) {
            ShowModel *s=[[ShowModel alloc]init];
            [s setValuesForKeysWithDictionary:arr2[i]];
            [self.dataArray addObject:s];
//            NSLog(@"poi_count=%@",s.poi_count);
//            NSLog(@"show_id=%@",s.show_id);
//            NSLog(@"ss.image_url=%@",s.image_url);
        }
        
        NSArray *arr3=responseBody[2][@"destinations"];
//        NSLog(@"arr3=%@",arr3);
        for (int i=0; i<arr3.count; i++) {
            ShowModel *s=[[ShowModel alloc]init];
            [s setValuesForKeysWithDictionary:arr3[i]];
            [self.dataArray addObject:s];
//            NSLog(@"poi_count=%@",s.poi_count);
//            NSLog(@"show_id=%@",s.show_id);
//            NSLog(@"ss.image_url=%@",s.image_url);
        }

        
        
        [self hidenLoadView];
        [_collectionView reloadData];
        
        
    } failureBlock:^(NSString *error) {
        NSLog(@"failureBlock");
        [self hidenLoadView];
    }];
}

-(void)loadData2
{
    
    [self showLoadView];
    
    [[NetworkSingleton sharedManager] getResultWithParameter:nil url:URL successBlock:^(id responseBody) {
        
        
        
        NSArray *array=responseBody[4][@"destinations"];
//        NSLog(@"array=%@",array);
        for (int i=0; i<array.count; i++) {
            ShowModel *sss=[[ShowModel alloc]init];
            [sss setValuesForKeysWithDictionary:array[i]];
            [self.dataArray addObject:sss];
//            NSLog(@"poi_count=%@",sss.poi_count);
//            NSLog(@"show_id=%@",sss.show_id);
//            NSLog(@"sss.image_url=%@",sss.image_url);
        }//for
        
        //
        //        NSArray *arr=responseBody[0][@"destinations"];
        //        NSLog(@"arr=%@",arr);
        //
        //       //通过字典数组，制作一个模型数组。
        //       _dataArray = [ShowModel objectArrayWithKeyValuesArray:[responseBody[0] objectForKey:@"destinations"]];
        
        
        NSArray *array2=responseBody[3][@"destinations"];
        NSLog(@"array2=%@",array2);
        for (int i=0; i<array2.count; i++) {
            ShowModel *sss=[[ShowModel alloc]init];
            [sss setValuesForKeysWithDictionary:array2[i]];
            [self.dataArray addObject:sss];
//            NSLog(@"poi_count=%@",sss.poi_count);
//            NSLog(@"show_id=%@",sss.show_id);
//            NSLog(@"sss.image_url=%@",sss.image_url);
        }
        
        
        [self hidenLoadView];
        [_collectionView reloadData];
        
        
        
    } failureBlock:^(NSString *error) {
        NSLog(@"failureBlock");
        [self hidenLoadView];
    }];
}


#pragma mark UICollectionViewDelegateFlowLayout

#pragma mark ZWwaterFlowDelegate
- (CGFloat)TYFlow:(TYCollectionViewLayout *)tyFlow heightForWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPach
{
    
    return 190*KWidth_Scale;
}
#pragma mark UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSLog(@"你点击了 %ld--%ld",(long)indexPath.section,indexPath.item);
    GBADetailViewController *detailVC = [[GBADetailViewController alloc]init];
    detailVC.aString = [[self.dataArray[indexPath.row] show_id] stringValue];
    detailVC.myModel=self.dataArray[indexPath.row];
    NSLog(@"%@",self.dataArray);
    [self.navigationController pushViewController:detailVC animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)hidenLoadView
{
    [UIView animateWithDuration:0.3 animations:^{
        
        [_LoadView removeFromSuperview];
        
    }];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)showLoadView
{
    _LoadView=[SDRotationLoopProgressView progressView];
    
    _LoadView.frame=CGRectMake(0, 0, 100*KWidth_Scale, 100*KWidth_Scale);
    
    _LoadView.center=self.view.center;
    
    [self.view addSubview: _LoadView ];
    
}







@end

