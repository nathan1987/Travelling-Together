//
//  MineViewController.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/26.
//  Copyright © 2015年 lanou3g. All rights reserved.
//
#import "ChangePWDViewController.h"
#import "CDUtils.h"
#import "ChangeUserNameViewController.h"
#import "MCPhotographyHelper.h"
#import "IATViewController.h"
#import <AVOSCloud/AVOSCloud.h>
#import "UserInfo.h"
#import "MineViewController.h"
#import "UserHandle.h"
#import "UserTableViewCell.h"
#import "LoginViewController.h"
@interface MineViewController ()<UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) MCPhotographyHelper *photographyHelper;
@property(nonatomic,strong)UITableView *tableview;
@end

@implementation MineViewController
- (MCPhotographyHelper *)photographyHelper {
    if (_photographyHelper == nil) {
        _photographyHelper = [[MCPhotographyHelper alloc] init];
    }
    return _photographyHelper;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self p_setupViews];
    
}


-(void)p_setupViews{
    
    
    
    CGRect rect1 =CGRectMake(0, 44, self.view.bounds.size.width, self.view.bounds.size.height);
    self.tableview =[[UITableView alloc]initWithFrame:rect1 style:UITableViewStyleGrouped];
    
    self.tableview.delegate = self;
    self.tableview.dataSource =self;
    [self.view addSubview:self.tableview];
    
    
    //set NavigationBar 背景颜色&title 颜色
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:16/255.0 green:135/255.0 blue:217/255.0 alpha:1.0]];
    CGRect rect = CGRectMake(0, 0, 188, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"我";
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"System Italic" size:18.0];
    self.navigationItem.titleView = label;
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    
    [self.tableview registerClass:[UserTableViewCell class] forCellReuseIdentifier:@"ucell"];
    
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    if ([UserHandle shareUser].isUserLogin) {
        label.text = @"我";
         self.navigationItem.titleView = label;
        [self.tableview reloadData];
    }
    else {
        
        
        label.text = @"未登录";
        self.navigationItem.titleView = label;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self p_setupViews];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableview
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
        if ([UserHandle shareUser].isUserLogin)
             return 120;
        else return 40;
            }
           
            break;
        case 1:
            return 40;
            break;
        case 2:
            return 40;
            break;
        default:
            return 0;
            break;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([UserHandle shareUser].isUserLogin) {
         return 3;
    }
    else
    {
        return 2;}
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
           return 1;
            break;
        case 2:
            return 1;
            break;
        default:
            return 0;
            break;
    }
    
    
    }
-(void)tapIcon:(UITapGestureRecognizer*)sender
{
    NSLog(@"更换头像");
    [self pickImage];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            
            if ([UserHandle shareUser].isUserLogin) {
                
                
                UserTableViewCell * ucell =[tableView dequeueReusableCellWithIdentifier:@"ucell" forIndexPath:indexPath];
                ucell.nameLabel.text=[AVUser currentUser].username;
                ucell.emailLabel.text=[UserHandle shareUser].myInfo.usr_email;
                ucell.myIMV.userInteractionEnabled=YES;
                UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapIcon:)];
                [ucell.myIMV addGestureRecognizer:singleTap1];
                NSLog(@"name=%@",[AVUser currentUser].username);
               
                if ([[AVUser currentUser] objectForKey:@"emailVerified"]) {
                    ucell.emailTestLabel.text=@"邮箱已验证";
                }else {
                 ucell.emailTestLabel.text=@"邮箱未验证";
                }
                
                
                //获取图片
                [[UserHandle shareUser] getAvatarImageOfUser:[AVUser currentUser] block:^(UIImage *image) {
                    if(image){
                        
                        CGFloat avatarWidth = 60;
                        CGSize avatarSize = CGSizeMake(avatarWidth, avatarWidth);
                        
                        UIImage *resizedImage = [self resizeImage:image toSize:avatarSize];
                        
                        
                        
                        ucell.myIMV.image=resizedImage;
                    }
                }];
                
                
                
                
                
                return ucell;
                
            }
            else
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                cell.textLabel.text = @"点击登录";
                return cell;
            }
        }
            break;
            
        case 1:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            
            switch (indexPath.row) {
                case 0:
                
                    
                    cell.textLabel.text = @"语音助手";
                
                    break;
                case 1:
                    
                    
                    cell.textLabel.text = @"实时汇率";
                    
                    break;
                case 2:
                    
                    
                    cell.textLabel.text = @"天气预报";
                    
                    break;
                default:
                    break;
            }
            
            
            return cell;
        
        
        
        
        }
            break;
            
        case 2:
        {
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.textLabel.text = @"退出登录";
            return cell;
            
            
            
            
        }
            break;
            
        default:
            return nil;
            break;
    }
    
    
    

    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            [self.tableview deselectRowAtIndexPath:indexPath animated:NO];
            if (![UserHandle shareUser].isUserLogin) {
                LoginViewController * loginVC = [[LoginViewController alloc]init];
                
                [self presentViewController:loginVC animated:YES completion:nil];
                
            }else{
                [self showEditActionSheet:nil];
            }
           
        
        }
            break;
            
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    IATViewController *iatVC=[[IATViewController alloc]init];
                    [self.navigationController pushViewController:iatVC animated:YES];
                
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
            
        case 2:
        {
            NSLog(@"点击退出登录");
            
            /*
            
            UIAlertController *alertC=[UIAlertController alertControllerWithTitle:@"确定要退出吗？" message:@"确定要退出吗？" preferredStyle:UIAlertControllerStyleAlert];

        */
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定要退出吗？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
            alertView.tag = indexPath.row;
            [alertView show];
            
           
        }
            break;
        default:
            break;
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
        //退出登录
        [AVUser logOut];  //清除缓存用户对象
        // AVUser *currentUser = [AVUser currentUser]; // 现在的currentUser是nil了
        [UserHandle shareUser].isUserLogin = NO;
        [UserHandle shareUser].myInfo=nil;
        [self p_setupViews];    }
}
//更改头像
-(void)pickImage {
    
    NSLog(@"图片");
    [self.photographyHelper showOnPickerViewControllerOnViewController:self completion:^(UIImage *image) {
        
        NSLog(@"处理图片");
        
       
        
        __weak typeof(self) weakself=self;
        if (image) {
            //UIImage *rounded = [CDUtils roundImage:image toSize:CGSizeMake(100, 100) radius:10];
            UIImage *rounded = [self roundImage:image toSize:CGSizeMake(100, 100) radius:10];
            
            [self showProgress];
            
            [[UserHandle shareUser] updateAvatarWithImage:rounded callback:^(BOOL succeeded, NSError *error) {
                [self hideProgress];
                [weakself .tableview reloadData];
            }];
            
            
//            [[CDUserManager manager] updateAvatarWithImage : rounded callback : ^(BOOL succeeded, NSError *error) {
//                [self hideProgress];
//                if ([self filterError:error]) {
//                    [self loadDataSource];
//                }
//            }];
        }
    }];
}
-(void)showProgress {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (UIImage *)roundImage:(UIImage *)image toSize:(CGSize)size radius:(float)radius;
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    [[UIBezierPath bezierPathWithRoundedRect:rect
                                cornerRadius:radius] addClip];
    [image drawInRect:rect];
    UIImage *rounded = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return rounded;
}





-(UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)newSize {
    
   
    
    
    
    
    
    
    
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}






#pragma mark 更新头像和资料
- (void)showEditActionSheet:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"更新资料" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"更改头像", @"更改用户名",@"更改密码", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    if (buttonIndex == 0) {
        [self pickImage];//更换头像
    } else if (buttonIndex == 1) {
        //更改用户名
        NSLog(@"更改用户名");
    
        ChangeUserNameViewController *changenameVC=[[ChangeUserNameViewController alloc]init];
        
        [self presentViewController:changenameVC animated:YES completion:nil];
    
    }else if (buttonIndex == 2) {
        //更改用户名
        NSLog(@"更改密码");
        
        ChangePWDViewController *changepwdVC=[[ChangePWDViewController alloc]init];
        
        [self presentViewController:changepwdVC animated:YES completion:nil];
        
    }
}



@end
