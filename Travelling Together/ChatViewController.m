//
//  Copyright © 2015年 lanou3g. All rights reserved.
//
#import "PeopleAroudViewController.h"
#import "RecentConversationsViewController.h"
#import "NewAddRequestFriendsListViewController.h"
#import "LoginViewController.h"
#import "UserHandle.h"
#import "ChatViewController.h"
#import <AVOSCloud/AVOSCloud.h>
#import "MyFriendListViewController.h"
#import <AVOSCloudIM/AVOSCloudIM.h>
#import "TomAndJerryEpisode.h"
#import "ConversationViewController.h"
#import "SearchFriendViewController.h"
#import "ChatTableViewCell.h"
#import "JSBadgeView.h"
@interface ChatViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableview;
@end

@implementation ChatViewController
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
  
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    if ([[UserHandle shareUser].jump isEqualToString:@"chat"]) {
        
        RecentConversationsViewController *r1VC=[[RecentConversationsViewController alloc]init];
        [self.navigationController pushViewController:r1VC animated:YES];
        
        
    }else if ([[UserHandle shareUser].jump isEqualToString:@"request"]){
        
        NewAddRequestFriendsListViewController *n1VC=[[NewAddRequestFriendsListViewController alloc]init];
        [self.navigationController pushViewController:n1VC animated:YES];
        
        
    }else{
    
    [self showProgress];
    __weak typeof(self) ws = self;
    [[UserHandle shareUser] checkMyAddRequestsAndAddNewWithBlock:^(NSArray *objects, NSError *error) {
        
        
        
        
        [ws hideProgress];
        [ws.tableview reloadData];
        
        
      
    }];
    }
    
    
    
//    if (![UserHandle shareUser].isUserLogin) {
//       // [self.tableview deselectRowAtIndexPath:indexPath animated:NO];
//        LoginViewController *loginVC = [[LoginViewController alloc]init];
//        [self presentViewController:loginVC animated:YES completion:nil];
//        
//    }
    
}

-(void)showProgress {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)addFriend:(UIBarButtonItem*)sender{
    NSLog(@"点击添加好友按钮。");
    if (![UserHandle shareUser].isUserLogin) {
        //[self.tableview deselectRowAtIndexPath:indexPath animated:NO];
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        [self presentViewController:loginVC animated:YES completion:nil];
        
    }else{
    SearchFriendViewController *searchVC = [[SearchFriendViewController alloc]init];
        [self.navigationController pushViewController:searchVC animated:YES];}
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    
    if (![UserHandle shareUser].isUserLogin) {
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        [self presentViewController:loginVC animated:YES completion:nil];
    }
    
   
   
   
    
//    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFriend:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"addnew_black"] style:UIBarButtonItemStylePlain target:self action:@selector(addFriend:)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    
    //set NavigationBar 背景颜色&title 颜色
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:16/255.0 green:135/255.0 blue:217/255.0 alpha:1.0]];
    self.tableview = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
   // self.tableview.backgroundColor = [UIColor whiteColor];
    self.tableview.delegate =self;
    self.tableview.dataSource = self;
    [self.view addSubview:self.tableview];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    //self.navigationController.title = @"朋友";
  //  self.navigationItem.title = @"朋友";
    CGRect rect = CGRectMake(0, 0, 188, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"朋友";
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"System Italic" size:18.0];
    self.navigationItem.titleView = label;
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableview registerClass:[ChatTableViewCell class] forCellReuseIdentifier:@"ccell"];
    
   
        
    
    
//    [self showProgress];
//    __weak typeof(self) ws = self;
//    [[UserHandle shareUser] checkMyAddRequestsAndAddNewWithBlock:^(NSArray *objects, NSError *error) {
//        [ws hideProgress];
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
       return 3;
    }else return 0;
   
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        
    
    
    ChatTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ccell" forIndexPath:indexPath];
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    static NSInteger kBadgeViewTag = 103;
    JSBadgeView *badgeView = (JSBadgeView *)[cell viewWithTag:kBadgeViewTag];
    if (badgeView) {
        [badgeView removeFromSuperview];
    }
    
    switch (indexPath.row) {
        case 0:
           // cell.textLabel.text = @"会话";
            cell.nameLabel.text=@"会话";
       cell.myIMV.image=[UIImage imageNamed:@"mychats.png"];
            break;
        case 1:
           // cell.textLabel.text = @"新的朋友";
            cell.nameLabel.text=@"新的朋友";
            cell.myIMV.image=[UIImage imageNamed:@"newmyfriend.png"];
        {
        
            if ([UserHandle shareUser].unreadAddRequest > 0) {
                badgeView = [[JSBadgeView alloc] initWithParentView:cell.myIMV alignment:JSBadgeViewAlignmentTopRight];
                badgeView.tag = kBadgeViewTag;
                badgeView.badgeText = [NSString stringWithFormat:@"%ld", [UserHandle shareUser].unreadAddRequest];
                NSLog(@"unreadAddRequest=%ld",[UserHandle shareUser].unreadAddRequest);
            }

        }
            break;
        case 2:
            //cell.textLabel.text = @"我的好友";
            cell.nameLabel.text=@"我的好友";
            cell.myIMV.image=[UIImage imageNamed:@"friend.png"];
        {
            if ([UserHandle shareUser].newFriend > 0) {
                badgeView = [[JSBadgeView alloc] initWithParentView:cell.myIMV alignment:JSBadgeViewAlignmentTopRight];
                badgeView.tag = kBadgeViewTag;
                badgeView.badgeText = [NSString stringWithFormat:@"%ld", [UserHandle shareUser].newFriend];
                  NSLog(@"newFriend=%ld",[UserHandle shareUser].unreadAddRequest);
            }
        }
            break;
        case 3:
            //cell.textLabel.text = @"我的小组";
            cell.nameLabel.text=@"我的小组";
            cell.myIMV.image=[UIImage imageNamed:@"mygroup.png"];
            break;
        case 4:
            //cell.textLabel.text = @"附近的人";
            cell.nameLabel.text=@"附近的人";
            cell.myIMV.image=[UIImage imageNamed:@"around.png"];
            break;
        case 5:
            //cell.textLabel.text = @"客服";
            cell.nameLabel.text=@"客服";
            break;
        default:
            break;
    }
    
    return cell;
    }
    else return nil;
    
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (![UserHandle shareUser].isUserLogin) {
        [self.tableview deselectRowAtIndexPath:indexPath animated:NO];
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        [self presentViewController:loginVC animated:YES completion:nil];
        
    }else{
    switch (indexPath.row) {
           case 0:
            
        {
            RecentConversationsViewController *recentVC=[[RecentConversationsViewController alloc]init];
            
            recentVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:recentVC animated:YES];
            
            
            
        }
            break;
        case 1:
        {
            NewAddRequestFriendsListViewController *newVC=[[NewAddRequestFriendsListViewController alloc]init];
            
            newVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:newVC animated:YES];
            
            // [self presentViewController:conVC animated:YES completion:nil];
        }
            break;
        case 2:
        {
            MyFriendListViewController *myVC=[[MyFriendListViewController alloc]init];
           
            myVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myVC animated:YES];
            
            // [self presentViewController:conVC animated:YES completion:nil];
        }
            break;
        case 3:
            
            break;
        case 4:
        {
            PeopleAroudViewController * peoVC = [[PeopleAroudViewController alloc]init];
            peoVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:peoVC animated:YES];
            
            // [self presentViewController:conVC animated:YES completion:nil];
        }
            break;
        case 5:
        {
            ConversationViewController * conVC = [[ConversationViewController alloc]init];
            conVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:conVC animated:YES];
        
           // [self presentViewController:conVC animated:YES completion:nil];
        }
            
            
            break;
        default:
            break;
    }
    
    }
    
}


-(void)showHUDText:(NSString*)text{
    MBProgressHUD* hud=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    hud.labelText=text;
    hud.detailsLabelFont = [UIFont systemFontOfSize:14];
    hud.detailsLabelText = text;
    hud.margin=10.f;
    hud.removeFromSuperViewOnHide=YES;
    hud.mode=MBProgressHUDModeText;
    [hud hide:YES afterDelay:2];
}

@end
