//
//  TomAndJerryEpisode.h
//  Travelling Together
//
//  Created by 李宁 on 15/11/30.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AVIMClient;

@interface TomAndJerryEpisode : NSObject
@property (nonatomic, strong) AVIMClient *client;
- (void)tomSendMessageToJerry;
- (void)jerryReceiveMessageFromTom;
- (void)tomQueryMessagesWithLimit;
@end
