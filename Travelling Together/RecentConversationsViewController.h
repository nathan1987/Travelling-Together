//
//  RecentConversationsViewController.h
//  Travelling Together
//
//  Created by 李宁 on 15/12/5.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVOSCloudIM/AVOSCloudIM.h>
@interface RecentConversationsViewController : UIViewController

@property(nonatomic,strong) AVUser *toWho;
@end
