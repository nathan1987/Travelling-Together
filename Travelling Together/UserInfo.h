//
//  UserInfo.h
//  Travelling Together
//
//  Created by 李宁 on 15/12/1.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UserInfo : NSObject
@property(nonatomic,strong) NSString *onlyID;//唯一的id
//昵称
@property(nonatomic,strong) NSString *usr_name;
//邮箱
@property(nonatomic,strong) NSString *usr_email;
//密码
@property(nonatomic,strong) NSString *usr_pwd;
//性别
@property(nonatomic,strong) NSString *usr_gender;
//年龄
@property(nonatomic,assign) NSInteger usr_age;
//用户头像
@property(nonatomic,strong) UIImage  *usr_icon;
//用户手机号
@property(nonatomic,strong) NSString *usr_phonenum;
@end
