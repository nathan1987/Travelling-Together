//
//  ConversationViewController.m
//  Travelling Together
//
//  Created by 李宁 on 15/12/1.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "ConversationViewController.h"

@interface ConversationViewController ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
//显示对话信息的tableview
@property(nonatomic,strong)UITableView *tableview;
//输入框
@property(nonatomic,strong)UITextView *textview;
@property(nonatomic,strong)UIButton *sendButton;
@property(nonatomic,strong)NSMutableArray *chatArray;
@end

@implementation ConversationViewController
#pragma mark 懒加载
-(NSMutableArray*)chatArray{
    if (_chatArray == nil) {
        _chatArray = [NSMutableArray array];
    }
    return _chatArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
  CGRect rect1 =CGRectMake(0, 44, self.view.bounds.size.width, self.view.bounds.size.height-244);
    self.tableview =[[UITableView alloc]initWithFrame:rect1 style:UITableViewStylePlain];
    
    self.tableview.delegate = self;
    self.tableview.dataSource =self;
    [self.view addSubview:self.tableview];
    
    
    
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    
    self.textview = [[UITextView alloc]init];
    CGRect rect2 =CGRectMake(0, CGRectGetMaxY(rect1)+10, self.view.bounds.size.width, 120);
    
    self.textview.frame =rect2;
    self.textview.backgroundColor = [UIColor yellowColor];
    
    self.textview.delegate = self;
    
    
    [self.view addSubview:self.textview];
    
    
    
    
    
    //定义一个toolBar

    UIToolbar
    * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    
    //设置style
    [topView
     setBarStyle:UIBarStyleBlack];
    
    //定义两个flexibleSpace的button，放在toolBar上，这样完成按钮就会在最右边
    UIBarButtonItem
    * button1 =[[UIBarButtonItem  alloc]initWithBarButtonSystemItem:                                        UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem
    * button2 = [[UIBarButtonItem  alloc]initWithBarButtonSystemItem:                                        UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
    
    //定义完成按钮
    
    UIBarButtonItem
    * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"完成"style:UIBarButtonItemStyleDone
                                                  target:self action:@selector(resignKeyboard)];
    
    
    
    //在toolBar上加上这些按钮
    
    NSArray
    * buttonsArray = [NSArray arrayWithObjects:button1,button2,doneButton,nil];     
    
    [topView
     setItems:buttonsArray];
    [self.textview
     setInputAccessoryView:topView];
    
    
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendButton setTitle:@"发送" forState:UIControlStateNormal];
    
    CGRect rect3 =CGRectMake(0, CGRectGetMaxY(rect2)+10, self.view.bounds.size.width, 50);
    self.sendButton.frame = rect3;
   [ self.sendButton setBackgroundColor:[UIColor greenColor]];
    [self.view addSubview:self.sendButton];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark tableview
//行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 100;
}
//cell内容
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = @"测试";
    return cell;
}

#pragma mark UITextViewDelegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGRect frame = textView.frame;
    //
    int offset = frame.origin.y + 200 - (self.view.frame.size.height - 216.0);//键盘高度216
    NSLog(@"offset=%d",offset);
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    //将视图的Y坐标向上移动offset个单位，以使下面腾出地方用于软键盘的显示
    if(offset > 0)
        self.view.frame = CGRectMake(0.0f, -offset, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}



-(void)textViewDidEndEditing:(UITextView *)textView{
    self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

#pragma mark 隐藏键盘
-(void)resignKeyboard
{
    
    [self.textview
     resignFirstResponder];
    
}
@end
