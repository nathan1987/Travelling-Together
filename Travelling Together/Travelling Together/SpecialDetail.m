//
//  SpecialDetail.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/11.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "SpecialDetail.h"

@implementation SpecialDetail

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"description"]) {
        self.mydescription = value;
    }
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"note"]) {
        self.trip_name = value[@"trip_name"];
        self.user_name = value[@"user_name"];
    }
}



@end
