//
//  TravellingViewController.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/26.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "TravellingViewController.h"
#import "TravellingCell.h"
#import "DownLoad.h"
#import "Single.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"
#import "TwoButtonView.h"
#import "SpecialTableViewCell.h"
#import "Special.h"
#import "SpecialDetailViewController.h"
#import "MJRefresh.h"
static const CGFloat MJDuration = 1.0;

@interface TravellingViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong)TwoButtonView * button;

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong)NSMutableArray *dataArray;

@property(nonatomic, strong)NSMutableArray * dataArray1;

@property (nonatomic, strong)UIScrollView * sv;

@property (nonatomic, strong)NSString * picID;

@property(nonatomic, strong)UITableView * tableView1;



@end

@implementation TravellingViewController

- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)dataArray1
{
    if (_dataArray1 == nil) {
        _dataArray1 = [NSMutableArray array];
    }
    return _dataArray1;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.button.frame) + 5, self.view.frame.size.width, self.view.frame.size.height)];
    self.sv.contentSize = CGSizeMake(self.view.frame.size.width * 2, 0);
    self.sv.delegate = self;
    [self.view addSubview:self.sv];
    self.sv.pagingEnabled = YES;
    self.sv.bounces = NO;

    
    self.navigationItem.title = @"同游记";
    
    //set NavigationBar 背景颜色&title 颜色
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:16/255.0 green:135/255.0 blue:217/255.0 alpha:1.0]];
    
    CGRect rect = CGRectMake(0, 0, 188, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"同游记";
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"System Italic" size:18.0];
    self.navigationItem.titleView = label;
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconfont-shoucangweishoucang"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemAction:)];
//    
//    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(self.button.frame) + 95, self.view.frame.size.width - 10, self.view.frame.size.height- 140) style:(UITableViewStylePlain)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.sv addSubview:self.tableView];
    // 下拉刷新
    self.tableView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            
            // 结束刷新
            [self.tableView.mj_header endRefreshing];
        });
    }];
    
    [self.self.tableView.mj_header beginRefreshing];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[TravellingCell class] forCellReuseIdentifier:@"cell"];
    
    
    
    
    
    [DownLoad downLoadWithURL:@"http://chanyouji.com/api/trips/featured.json?page=1" method:@"GET" param:nil passValue:^(id data) {
        NSArray * array = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingAllowFragments) error:nil];
//        NSLog(@"%@",array);
//        NSLog(@"%@",array[0][@"id"]);
        self.picID = array[0][@"id"];

        for (NSDictionary * dic in array) {
            Single * s = [[Single alloc] init];
            [s setValuesForKeysWithDictionary:dic];
            [self.dataArray addObject:s];
     
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
    }];
    self.tableView1 = [[UITableView alloc] initWithFrame:CGRectMake(self.view.frame.size.width + 5, CGRectGetMaxY(self.button.frame) + 90, self.view.frame.size.width - 10, self.view.frame.size.height - 140) style:(UITableViewStylePlain)];
    self.tableView1.backgroundColor = [UIColor clearColor];
    [self.sv addSubview:self.tableView1];
    // 下拉刷新
    self.tableView1.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView1 reloadData];
            
            // 结束刷新
            [self.tableView1.mj_header endRefreshing];
        });
    }];
    
    [self.self.tableView.mj_header beginRefreshing];
    self.tableView1.delegate = self;
    self.tableView1.dataSource = self;
    
    [self.tableView1 registerClass:[SpecialTableViewCell class] forCellReuseIdentifier:@"cell1"];
    
    [DownLoad downLoadWithURL:@"http://chanyouji.com/api/articles.json?page=1" method:@"GET" param:nil passValue:^(id data) {
        NSArray * array1 = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingAllowFragments) error:nil];
        
        for (NSDictionary * dic1 in array1) {
            Special * s = [[Special alloc] init];
            [s setValuesForKeysWithDictionary:dic1];
            [self.dataArray1 addObject:s];
            NSLog(@"self.dataArray1==========%@", self.dataArray1);
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView1 reloadData];
        });
    }];
    
    self.button = [[TwoButtonView alloc] initWithFrame:CGRectMake(5, 65, self.view.frame.size.width, 30)];
    typeof(self) pSelf = self;
    self.button.myBlock = ^(NSInteger index) {
        [pSelf.sv setContentOffset:CGPointMake(index * self.view.frame.size.width, 0) animated:YES];
    };
    [self.view addSubview:self.button];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//// 抽屉效果收藏
//- (void)leftBarButtonItemAction:(UIBarButtonItem *)sender
//{
//    NSLog(@"我的收藏抽屉");
//}

#pragma mark - Tableview data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView ==  self.tableView) {
        return self.dataArray.count;
    } else
    {
        return self.dataArray1.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView  == self.tableView) {
        
        TravellingCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        // 创建一个实例对象去接收数组里的数据
        Single * s = self.dataArray[indexPath.row];
        
        cell.titleLabel.text = s.name;
        cell.titleLabel.textColor = [UIColor whiteColor];
        cell.dateLabel.font = [UIFont systemFontOfSize:12];
        NSArray *arr = [s.start_date componentsSeparatedByString:@"-"];
        cell.dateLabel.text = [NSString stringWithFormat:@"%@.%@.%@/%@天/%@图",arr[0], arr[1], arr[2],s.days,s.photos_count];
        cell.dateLabel.textColor = [UIColor whiteColor];
        
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:s.front_cover_photo_url]];
        
        return cell;

    }else
    {
        SpecialTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        Special * s = self.dataArray1[indexPath.row];
        NSLog(@"self.dataArray1[indexPath.row]===========%@",self.dataArray1[indexPath.row]);
        cell.titleLabel.text = s.name;
        cell.titleLabel.textColor = [UIColor whiteColor];
        cell.descripeLabel.text = s.title;
        cell.descripeLabel.textColor = [UIColor whiteColor];
        cell.descripeLabel.font = [UIFont systemFontOfSize:14.f];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:s.image_url]];
//        NSLog(@"s.image_url======%@",s.image_url);
        return cell;


    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 220;
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    CGFloat myFloat = scrollView.contentOffset.x;
//    
//    NSInteger index = myFloat / self.view.frame.size.width;
//    
//    
//    [self.button setButtonClicked:index];
//}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView) {
        DetailViewController * DVC = [[DetailViewController alloc] init];
        DVC.aString = [[self.dataArray[indexPath.row] f_id] stringValue];
        DVC.imageString = [self.dataArray[indexPath.row] front_cover_photo_url];
        DVC.titleString = [self.dataArray[indexPath.row] name];
        [self.navigationController pushViewController:DVC animated:YES];
    } else {
        SpecialDetailViewController * SVC = [[SpecialDetailViewController alloc] init];
        SVC.bString = [[self.dataArray1[indexPath.row] myid] stringValue];
        SVC.imageString = [self.dataArray1[indexPath.row] image_url];
        SVC.titleString = [self.dataArray1[indexPath.row] name];
        [self.navigationController pushViewController:SVC animated:YES];
//        NSLog(@"123");
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
