//
//  DetailViewController.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailTableViewCell.h"
#import "Detail2TableViewCell.h"
#import "Detail.h"
#import "UIImageView+WebCache.h"
#import "UMSocial.h"
#import "MJRefresh.h"
static const CGFloat MJDuration = 1.0;

@interface DetailViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong)NSMutableArray *dataArray;

@property (nonatomic, strong)UIImageView * headView;

@property (nonatomic, strong)UIButton * shoucang;






@end

@implementation DetailViewController
- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    self.headView.backgroundColor = [UIColor yellowColor];
    [self.headView sd_setImageWithURL:[NSURL URLWithString:self.imageString]];
    self.headView.userInteractionEnabled = YES;
    CGRect rect = CGRectMake(0, 0, 188, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.backgroundColor = [UIColor clearColor];
    label.text = self.titleString;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"System Italic" size:10.f];
    self.navigationItem.titleView = label;

    
//    self.shoucang = [UIButton buttonWithType:(UIButtonTypeSystem)];
//    self.shoucang.frame = CGRectMake(CGRectGetWidth(self.headView.frame) - 60, 10, 40, 40);
//    self.shoucang.backgroundColor = [UIColor clearColor];
//    
//    [self.shoucang setBackgroundImage:[UIImage imageNamed:@"iconfont-shoucangweishoucang"] forState:(UIControlStateNormal)];
//    [self.shoucang addTarget:self action:@selector(shoucangAction:) forControlEvents:(UIControlEventTouchUpInside)];
//
//    [self.headView addSubview:self.shoucang];

    self.view.backgroundColor = [UIColor clearColor];
    UIBarButtonItem * b2 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconfont-fenxiang"]style:(UIBarButtonItemStyleDone) target:self action:@selector(b2Action:)];
    b2.tintColor = [UIColor whiteColor];

    self.navigationItem.rightBarButtonItem = b2;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"iconfont-fanhui"] style:(UIBarButtonItemStylePlain) target:self action:@selector(leftBarAction:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
   
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width - 10, self.view.frame.size.height) style:(UITableViewStylePlain)];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.tableHeaderView = self.headView;
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    // 下拉刷新
    self.tableView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            
            // 结束刷新
            [self.tableView.mj_header endRefreshing];
        });
    }];

    [self.self.tableView.mj_header beginRefreshing];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerClass:[DetailTableViewCell class] forCellReuseIdentifier:@"DetailCell"];
    
    [self.tableView registerClass:[Detail2TableViewCell class] forCellReuseIdentifier:@"Detail2Cell"];
    

    NSString * s= [NSString stringWithFormat:@"http://chanyouji.com/api/trips/%@.json", self.aString];
    
    NSURL * url  = [NSURL URLWithString:s];
    
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingAllowFragments) error:nil];
        
        NSMutableArray * array = dic[@"trip_days"];
        
        for (NSDictionary * dic1 in array) {
            
            NSMutableArray * array1 = dic1[@"nodes"];
            
            for (NSDictionary * dic2 in array1) {
                
                NSMutableArray * array2 = dic2[@"notes"];
                for (NSDictionary * dic3 in array2) {
                    Detail * d = [[Detail alloc] init];
                    
                    [d setValuesForKeysWithDictionary:dic3];
                    
                    [self.dataArray addObject:d];
//                    NSLog(@"pic_url=%@",d.pic_url);
                    //NSLog(@"d.mydescription=%@",d.mydescription);
                }
    
            }
   
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // 重新载入数据
            [self.tableView reloadData];
            //NSLog(@"self.dataArray=%@", self.dataArray);

        });

        
    }];
    
    [task resume];

}
//- (void)shoucangAction:(UIButton *)sender
//{
//    if (sender.selected == NO) {
//        
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"收藏成功" preferredStyle:(UIAlertControllerStyleAlert)];
//        UIAlertAction * enter = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
//            [self.shoucang setBackgroundImage:[UIImage imageNamed:@"iconfont-shoucangyishoucang"] forState:(UIControlStateNormal)];
//        }];
//
//        [alert addAction:enter];
//        [self presentViewController:alert animated:YES completion:^{
//            
//         }];
//
//     }
//    
//
//
//}

- (void)leftBarAction:(UIBarButtonItem *)sender
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)b2Action:(UIButton *)sender
{
    NSLog(@"分享");
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:@"56667c8167e58e8c100016db"
                                      shareText:@"你要分享的文字"
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToYXSession,UMShareToSms,UMShareToTwitter,UMShareToRenren,nil]
                                       delegate:nil];
}
//- (void)b3Action:(UIButton *)sender
//{
//    
//    if (sender.selected) {
//        
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"标记成功" preferredStyle:(UIAlertControllerStyleAlert)];
//        UIAlertAction * enter = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
//            [sender setImage:[UIImage imageNamed:@"iconfont-shoucangyishoucang"] forState:(UIControlStateNormal)];
//            
//        }];
////        UIAlertAction * cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
////            [sender setImage:[UIImage imageNamed:@"iconfont-shoucangweishoucang"] forState:(UIControlStateNormal)];
////        }];
////
////        [alert addAction:cancle];
//        [alert addAction:enter];
//
//        [self presentViewController:alert animated:YES completion:^{
//            
//        }];
//        sender.selected = NO;
//
//    } else {
//        
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"是否取消标记" preferredStyle:(UIAlertControllerStyleAlert)];
//        UIAlertAction * enter = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
//            [sender setImage:[UIImage imageNamed:@"iconfont-shoucangweishoucang"] forState:(UIControlStateNormal)];
//        }];
////        UIAlertAction * cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
////            [sender setImage:[UIImage imageNamed:@"iconfont-shoucangyishoucang"] forState:(UIControlStateNormal)];
////        }];
//        [alert addAction:enter];
////        [alert addAction:cancle];
//        [self presentViewController:alert animated:YES completion:^{
//            
//        }];
//     
//    }
//     sender.selected = YES;
//}



#pragma mark -- TableView代理方法

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    Detail * d = self.dataArray[indexPath.row];

    if (d.pic_url == nil) {
        DetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell" forIndexPath:indexPath];
    
        cell.detailLabel.text = d.mydescription;
        return cell;
    } else {
    Detail2TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Detail2Cell" forIndexPath:indexPath];
    cell.footLabel.text = d.mydescription;
        [cell.imv sd_setImageWithURL:[NSURL URLWithString:d.pic_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
           dispatch_async(dispatch_get_main_queue(), ^{
               cell.imv.image = image;
           });
            
        }];
        

    return cell;
    }
    
    return nil;
  
}

// cell高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 拿到获取高度的字符串
    NSString * s = [self.dataArray[indexPath.row] mydescription];
    if ([self.dataArray[indexPath.row] pic_url] == nil) {
        return [DetailTableViewCell detailLabelCellHeight:[self heightWithString:s]] + 10;
    } else {
        return [Detail2TableViewCell detail2LabelCellHeight:[self heightWithString:s]] + 280;
    }
}

// 设置方法计算字符串高度
- (CGFloat)heightWithString:(NSString *)aString
{
    CGRect r = [aString boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.tableView.frame) - 10, 2000)  options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18.f]} context:nil];
    
    
    return r.size.height;
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
