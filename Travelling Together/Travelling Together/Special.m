//
//  Special.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/5.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "Special.h"

@implementation Special

- (void)setValue:(id)value forKey:(NSString *)key
{
    
    [super setValue:value forKey:key];
    
    
    if ([key isEqualToString:@"id"]) {
        self.myid = value;
    }
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}
@end
