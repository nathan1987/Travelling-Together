//
//  TravellingCell.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "TravellingCell.h"

@implementation TravellingCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.imgView = [[UIImageView alloc] init];
        [self.contentView addSubview:_imgView];
        
        self.titleLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_titleLabel];
        
        self.dateLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_dateLabel];


        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imgView.frame = CGRectMake(CGRectGetMinX(self.contentView.frame) + 5, CGRectGetMinY(self.contentView.frame), CGRectGetWidth(self.contentView.frame) - 5, CGRectGetHeight(self.contentView.frame) - 5);
//        self.imgView.backgroundColor = [UIColor yellowColor];
    
    self.titleLabel.frame = CGRectMake(5, CGRectGetMinY(self.imageView.frame) + 10, 300, 30);
//        self.titleLabel.backgroundColor = [UIColor blueColor];
    
    self.dateLabel.frame = CGRectMake(5,CGRectGetMaxY(self.titleLabel.frame) + 5, 300, 20);
//    self.dateLabel.backgroundColor = [UIColor redColor];
    
    
}

+(CGFloat)cellHeight
{
    return 120;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
