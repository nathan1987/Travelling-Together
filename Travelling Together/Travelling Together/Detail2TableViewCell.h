//
//  Detail2TableViewCell.h
//  Travelling Together
//
//  Created by lanou3g on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Detail2TableViewCell : UITableViewCell

@property(nonatomic, strong)UIImageView * imv;
@property(nonatomic, strong)UILabel * footLabel;


+ (CGFloat)detail2LabelCellHeight:(CGFloat)height;

@end
