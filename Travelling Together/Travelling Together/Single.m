//
//  Single.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "Single.h"
#import "DownLoad.h"

@implementation Single

-(void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    
    if ([key isEqualToString:@"id"]) {
        
        self.f_id = value;
    }
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"name"]) {
        self.name = value[@"name"];
    }
}
//用来记录被归档对象属性
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.photos_count forKey:@"photos_count"];
    [aCoder encodeObject:self.start_date forKey:@"start_date"];
    [aCoder encodeObject:self.end_date forKey:@"end_date"];
    [aCoder encodeObject:self.days forKey:@"days"];
    [aCoder encodeObject:self.front_cover_photo_url forKey:@"front_cover_photo_url"];
    [aCoder encodeObject:self.headImage forKey:@"image"];
    
    
}

//重新获取归档对象的属性值
- (id)initWithCoder:(NSCoder *)fDecoder
{
    self = [super init];
    if (self) {
        self.name = [fDecoder decodeObjectForKey:@"name"];
        self.photos_count = [fDecoder decodeObjectForKey:@"photos_count"];
        self.start_date = [fDecoder decodeObjectForKey:@"start_date"];
        self.end_date = [fDecoder decodeObjectForKey:@"end_date"];
        self.days = [fDecoder decodeObjectForKey:@"days"];
        self.front_cover_photo_url = [fDecoder decodeObjectForKey:@"front_cover_photo_url"];
        self.headImage = [fDecoder decodeObjectForKey:@"image"];
    }
    return self;
    
}


@end
