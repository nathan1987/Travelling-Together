//
//  Single.h
//  Demo
//
//  Created by 段凯 on 15/12/7.
//  Copyright © 2015年 段凯. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DetailSingle : NSObject

@property (nonatomic,assign)NSNumber *special_id;
@property(nonatomic, copy)NSString * mydestinations;

@property (nonatomic,copy)NSString *image_url;
@property (nonatomic,copy)NSString *photo_image_url;
@property (nonatomic,copy)NSString *name_zh_cn;
@property (nonatomic,copy)NSString *name_en;
@property (nonatomic,strong)UIImage *photo_image;

@end
