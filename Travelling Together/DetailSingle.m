//
//  Single.m
//  Demo
//
//  Created by 段凯 on 15/12/7.
//  Copyright © 2015年 段凯. All rights reserved.
//

#import "DetailSingle.h"

@implementation DetailSingle

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"destinations"]) {
        self.mydestinations = value[@"destinations"];
    }
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"image_url"]) {
        self.photo_image_url = value ;
    }

}

//用来记录被归档对象属性
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name_zh_cn forKey:@"name_zh_cn"];
    [aCoder encodeObject:self.name_en forKey:@"name_en"];
    [aCoder encodeObject:self.photo_image_url forKey:@"image_url"];
    [aCoder encodeObject:self.special_id forKey:@"special_id"];
    [aCoder encodeObject:self.photo_image forKey:@"photo_image"];
    
}

//重新获取归档对象的属性值
- (id)initWithCoder:(NSCoder *)fDecoder
{
    self = [super init];
    if (self) {
        self.name_zh_cn = [fDecoder decodeObjectForKey:@"name_zh_cn"];
        self.name_en = [fDecoder decodeObjectForKey:@"name_en"];
        self.photo_image_url = [fDecoder decodeObjectForKey:@"image_url"];
        self.special_id = [fDecoder decodeObjectForKey:@"special_id"];
        self.photo_image = [fDecoder decodeObjectForKey:@"photo_image"];
    }
    return self;
    
}


@end
