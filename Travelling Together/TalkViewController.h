//
//  TalkViewController.h
//  Travelling Together
//
//  Created by 李宁 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AVIMConversation;
@class AVUser;
@interface TalkViewController : UIViewController
@property(nonatomic,strong) AVUser *toWho;
@property (nonatomic, strong) AVIMConversation *conversation;
@property(nonatomic,assign)BOOL isFirstTalk;
@end
