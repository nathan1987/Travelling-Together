//
//  PeopleAroudViewController.m
//  Travelling Together
//
//  Created by 李宁 on 15/12/11.
//  Copyright © 2015年 lanou3g. All rights reserved.
//
#import <MAMapKit/MAMapKit.h>
#import "PeopleAroudViewController.h"

@interface PeopleAroudViewController ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate>
@property(nonatomic,strong)NSMutableArray *peopleArray;
@property(nonatomic,strong)UITableView *tableview;
@property(nonatomic,strong)MAMapView *myMap;
@property(nonatomic,assign)BOOL isMap;

@property(nonatomic,assign)BOOL isMapAdd;
@end

@implementation PeopleAroudViewController
-(MAMapView*)myMap{
    if (_myMap==nil) {
        _myMap=[[MAMapView alloc]initWithFrame:self.view.bounds];
        _myMap.showsUserLocation=YES;
        //_myMap.delegate=self;
    }
    return _myMap;
}
-(NSMutableArray*)peopleArray{
    if (_peopleArray==nil) {
        _peopleArray=[NSMutableArray arrayWithCapacity:10];
    }
    return _peopleArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   //self.automaticallyAdjustsScrollViewInsets=YES;
   self.title=@"附近的人";
    self.tableview=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.tableview.delegate=self;
    self.tableview.dataSource=self;
   //
    
    [self.view addSubview:self.tableview];
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(gotoMapAction:)];
[self.navigationItem.rightBarButtonItem setTitle:@"切换到地图"];


}
-(void)gotoMapAction:(UIBarButtonItem*)sender{
    
    
    if (!self.isMap) {
        NSLog(@"跳转到地图");
        self.isMap=YES;
        
        [self.navigationItem.rightBarButtonItem setTitle:@"切换到列表"];
        if (!self.isMapAdd) {
            
            self.myMap.delegate=self;
            self.myMap.showsUserLocation=YES;
            [self.view addSubview:self.myMap];
        }
        else{
            [self.view exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
        }
        
        
         }
    else{
        NSLog(@"跳转到列表");
        
        [self.navigationItem.rightBarButtonItem setTitle:@"切换到地图"];
        self.isMap=NO;
        //[self.view exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
        [self.view exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
        
        //[self.myMap removeFromSuperview];
    }
  
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableViewDelegate
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text=@"测试";
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return self.peopleArray.count;
    return 5;
}

#pragma mark mapView
-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation
updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    }
}


@end
