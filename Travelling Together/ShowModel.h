//
//  ShowModel.h
//  Travelling Together
//
//  Created by 段凯 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowModel : NSObject

@property (nonatomic,strong)NSNumber *category;

@property (nonatomic,strong)NSDictionary *destination;

@property (nonatomic,strong)NSNumber *show_id;

@property (nonatomic,strong)NSString *name_zh_cn;

@property (nonatomic,strong)NSString *name_en;

@property (nonatomic,strong)NSString *poi_count;

@property (nonatomic,strong)NSString *image_url;

-(id)initWithDictionary:(NSDictionary *)dictionary;

@end
