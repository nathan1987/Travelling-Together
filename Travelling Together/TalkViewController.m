//
//  TalkViewController.m
//  Travelling Together
//
//  Created by 李宁 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//
#import "UserHandle.h"
#import "FCMessageCell.h"
#import "AVUserStore.h"
#import "UserProfile.h"
#import "Message.h"
#import "myConversations.h"
#import "TalkViewController.h"
#import "UUInputFunctionView.h"
@interface TalkViewController ()<AVIMClientDelegate,UITableViewDataSource,UITableViewDelegate,UUInputFunctionViewDelegate,IMEventObserver,ConversationOperationDelegate,FCMessageCellDelegate>
@property(nonatomic,strong)UITableView *tableview;
@property(nonatomic,strong)UUInputFunctionView *myInputview;

@property (nonatomic, strong) NSMutableArray *messagesArray;
@end

@implementation TalkViewController

-(NSMutableArray*)messagesArray{
    if (_messagesArray==nil) {
        _messagesArray=[NSMutableArray arrayWithCapacity:10];
    }
    return _messagesArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    self.title = @"聊天";
    NSString *s=[NSString stringWithFormat:@"和%@聊天",self.toWho.username];
    self.title=s;
    CGRect rect1 = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 40);
    
    self.tableview = [[UITableView alloc]initWithFrame:rect1 style:UITableViewStylePlain];
    //去掉分割线
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.tableview.delegate=self;
    self.tableview.dataSource=self;
    [self.view addSubview:self.tableview];
    [self.tableview registerClass:[FCMessageCell class] forCellReuseIdentifier:@"CellID"];
    
    
    self.myInputview=[[UUInputFunctionView alloc]initWithSuperVC:self];
    self.myInputview.delegate=self;
    
    [self.view addSubview:self.myInputview];
    
    
    __weak typeof(self) weakSelf = self;
    
    //获取当前的会话
    myConversations *myCon=[myConversations sharedMyConversations];
    //设置代理
    myCon.myClient.delegate=self;
    
    //查询会话中的历史消息-比当前时间早，15条
    [myCon queryMoreMessages:self.conversation from:@"" timestamp:[[NSDate date] timeIntervalSince1970]*1000 limit:15 callback:^(NSArray *objects, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //获取到的消息，加到数组中
            [weakSelf.messagesArray addObjectsFromArray:objects];
            //刷新tableview
            [weakSelf.tableview reloadData];
            //滚动到最后一条
            if (weakSelf.messagesArray.count > 1) {
                [weakSelf.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:weakSelf.messagesArray.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            }
            if(self.isFirstTalk){
            [weakSelf UUInputFunctionView:weakSelf.myInputview  sendMessage:@"我们现在是好友了，可以聊天了！"];
            }
            
        });//dispatch_async
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.messagesArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    FCMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (cell == nil) {
        cell = [[FCMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
        cell.delegate = self;
    }
    FCMessageFrame *frame = [[FCMessageFrame alloc] init];
    [frame setMessage:((Message*)self.messagesArray[indexPath.row])];
    [cell setMessageFrame:frame];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    FCMessageFrame *frame = [[FCMessageFrame alloc] init];
    [frame setMessage:((Message*)self.messagesArray[indexPath.row])];
    return frame.cellHeight;
}
#pragma UUInputFunctionViewDelegate
// text
- (void)UUInputFunctionView:(UUInputFunctionView *)funcView sendMessage:(NSString *)message {
    if ([message length] > 0) {
        AVIMTextMessage *avMessage = [AVIMTextMessage messageWithText:message attributes:nil];
        NSLog(@"text=%@",avMessage.text);
        __weak typeof(self) weakSelf = self;
        [self.conversation sendMessage:avMessage callback:^(BOOL succeeded, NSError *error) {
            
            
            
            
            
            if (error) {NSLog(@"error=%@",error);
            } else {
                
                [[UserHandle shareUser]pushMessage:message userIds:@[self.toWho.objectId]  block:^(BOOL succeeded, NSError *error) {
                    if(succeeded) {NSLog(@"发送推送成功");}
                    else{
                    NSLog(@"发送推送失败");
                    
                    }
                }];
                
                
                [[myConversations sharedMyConversations] newMessageSent:avMessage conversation:weakSelf.conversation];
                
                Message *msg = [[Message alloc] initWithAVIMMessage:avMessage];
                [weakSelf.messagesArray addObject:msg];
                [weakSelf.tableview reloadData];
                [weakSelf.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:weakSelf.messagesArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            
            }
            
            funcView.TextViewInput.text =nil;
            [funcView changeSendBtnWithPhoto:YES];
        }];
    }
}
// image
- (void)UUInputFunctionView:(UUInputFunctionView *)funcView sendPicture:(UIImage *)image {
    if (image) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Image.jpg"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:filePath error:NULL];
        [UIImageJPEGRepresentation(image, 0.6) writeToFile:filePath atomically:YES];
        
        AVIMImageMessage *avMessage = [AVIMImageMessage messageWithText:nil attachedFilePath:filePath attributes:nil];
        
        
        __weak typeof(self) weakSelf = self;
        
        [self.conversation sendMessage:avMessage callback:^(BOOL succeeded, NSError *error) {
            if (error) {
               NSLog(@"error=%@",error);
            } else {
                [[myConversations sharedMyConversations] newMessageSent:avMessage conversation:self.conversation];
                Message *msg = [[Message alloc] initWithAVIMMessage:avMessage];
                [weakSelf.messagesArray addObject:msg];
                [weakSelf.tableview reloadData];
                [weakSelf.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:weakSelf.messagesArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
        }];
    }
}

// audio
- (void)UUInputFunctionView:(UUInputFunctionView *)funcView sendVoice:(NSData *)voice time:(NSInteger)second {
    if (voice) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"audio.mp3"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:filePath error:NULL];
        [voice writeToFile:filePath atomically:YES];
        
        AVIMAudioMessage *avMessage = [AVIMAudioMessage messageWithText:nil attachedFilePath:filePath attributes:nil];
        
        
        __weak typeof(self) weakSelf = self;
        
        [self.conversation sendMessage:avMessage callback:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"error=%@",error);
            } else {
                [[myConversations sharedMyConversations] newMessageSent:avMessage conversation:self.conversation];
                Message *msg = [[Message alloc] initWithAVIMMessage:avMessage];
                [weakSelf.messagesArray addObject:msg];
                [weakSelf.tableview reloadData];
                [weakSelf.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:weakSelf.messagesArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
        }];
    }
}


#pragma mark  IMEventObserver
- (void)newMessageArrived:(Message*)message conversation:(AVIMConversation*)conv {
  
    NSLog(@"新的消息");
    
    if (message.eventType == EventKicked) {
        UserProfile *profile = [[AVUserStore sharedInstance] getUserProfile:message.byClient];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ 剔除了你", profile.nickname] delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        [self exitConversation:conv];
        return;
    } else {
        NSLog(@"有新的消息到达");
        
        [self.messagesArray addObject:message];
        
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableview reloadData];
            [weakSelf.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:weakSelf.messagesArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        });
    }
}

- (void)messageDelivered:(Message*)message conversation:(AVIMConversation*)conversation {
    NSLog(@"发送");
}
#pragma FCMessageCellDelegate
- (void)headImageDidClick:(FCMessageCell *)cell userId:(NSString *)userId {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Tip" message:@"HeadImageClick !!!" delegate:nil cancelButtonTitle:@"sure" otherButtonTitles:nil];
    [alert show];
}
#pragma mark - AVIMClientDelegate

// 接收消息的回调函数--接收文本消息
- (void)conversation:(AVIMConversation *)conversation didReceiveTypedMessage:(AVIMTypedMessage *)message {
    
    
    
    NSLog(@"%@", message); // 耗子，起床！
    NSLog(@"AVIMClientDelegate");
    Message *m =[[Message alloc]initWithAVIMMessage:message];
  
    [self.messagesArray addObject:m];
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.tableview reloadData];
        [weakSelf.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:weakSelf.messagesArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    });
}


@end
