//
//  TYCollectionViewLayout.h
//  Travelling Together
//
//  Created by 段凯 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TYCollectionViewLayout;

@protocol TYFlowDelegate <NSObject>

- (CGFloat)TYFlow:(TYCollectionViewLayout *)tyFlow heightForWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPach;

@end


@interface TYCollectionViewLayout : UICollectionViewLayout

@property(nonatomic,assign)UIEdgeInsets sectionInset;
@property(nonatomic,assign)CGFloat rowMagrin;
@property(nonatomic,assign)CGFloat colMagrin;
@property(nonatomic,assign)CGFloat colCount;
@property(nonatomic,weak)id<TYFlowDelegate>degelate;

@end
