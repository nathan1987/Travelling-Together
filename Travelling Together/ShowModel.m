//
//  ShowModel.m
//  Travelling Together
//
//  Created by 段凯 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "ShowModel.h"

@implementation ShowModel

-(void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    
    if ([key isEqualToString:@"id"]) {
        
        self.show_id = value;
    }
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"name_zh_cn"]) {
        self.name_zh_cn = value[@"name_zh_cn"];
    }
}

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    if (self=[super init]) {
        
        self.show_id = dictionary[@"id"];
        self.name_zh_cn = dictionary[@"name_zh_cn"];
        self.name_en = dictionary[@"name_en"];
        self.poi_count = dictionary[@"poi_count"];
        self.image_url = dictionary[@"image_url"];
        
    }
    
    return self;
}


@end
