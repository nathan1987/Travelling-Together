//
//  MyFriendListViewController.m
//  Travelling Together
//
//  Created by 李宁 on 15/12/1.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "UIImageView+WebCache.h"
#import "FriendTableViewCell.h"
#import "UserHandle.h"
#import "TalkViewController.h"
#import "MyFriendListViewController.h"
#import <AVOSCloud/AVOSCloud.h>
#import <AVOSCloudIM/AVOSCloudIM.h>
#import "myConversations.h"
@interface MyFriendListViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property(nonatomic,strong)UITableView *tableview;
@property(nonatomic,strong)NSMutableArray *listArray;

@property(nonatomic,strong) AVUser *toWho;
@end

@implementation MyFriendListViewController

-(NSMutableArray*)listArray{
        if (_listArray==nil) {
            _listArray=[NSMutableArray arrayWithCapacity:10];
        }
    
    return _listArray;
}

-(void)showProgress {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)getFriendList{
    AVQuery *query = [AVUser query];
    //AVQuery *query = [AVQuery queryWithClassName:@"_User"];
    
    [query addAscendingOrder:@"username"];
    [query whereKey:@"objectId" notEqualTo:[AVUser currentUser].objectId];
    NSLog(@"id=%@",[AVUser currentUser].objectId);
    query.limit = 100;
    __weak typeof(self) ws = self;
    
    [self showProgress];
    [[UserHandle shareUser] findFriendsWithBlock:^(NSArray *objects, NSError *error) {
        if (error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [ws.listArray removeAllObjects];
                [ws.listArray addObjectsFromArray:objects];
                [ws.tableview reloadData];
                NSLog(@"获取好友objects=%@",objects);
                [ws hideProgress];
            });
            
        } else {
            [ws hideProgress];
            NSLog(@"获取好友error=%@",error);
        }
    }];
    
    
    



}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    self.tableview=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableview.delegate=self;
    self.tableview.dataSource=self;
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableview registerClass:[FriendTableViewCell class] forCellReuseIdentifier:@"fcell"];
    
    [self.view addSubview:self.tableview];

    [self getFriendList];
}

   - (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.listArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AVUser *u=self.listArray[indexPath.row];
    
    /*
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
 
    
    cell.textLabel.text=u.username;
    */
    
    FriendTableViewCell *fcell=[tableView dequeueReusableCellWithIdentifier:@"fcell" forIndexPath:indexPath];
    fcell.nameLabel.text=u.username;
    
    [[UserHandle shareUser]getAvatarImageOfUser:u block:^(UIImage *image) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            fcell.myIMV.image=image;
        });
        
    }];
    
    return fcell;
   
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    //向谁聊天
    AVUser *chatToUser = self.listArray[indexPath.row];
    self.toWho=chatToUser;
    //自己
    AVUser *currentUser = [AVUser currentUser];
    //聊天的数组
    NSArray *clientIds = [[NSArray alloc] initWithObjects:currentUser.objectId, chatToUser.objectId, nil];
    //AVIMClient
    AVIMClient *myClient = [[myConversations sharedMyConversations] myClient];
    NSLog(@"myClient=%@",myClient);
    AVIMConversationQuery *query = [myClient conversationQuery];
    query.limit = 10;
    query.skip = 0;
    [query whereKey:kAVIMKeyMember containsAllObjectsInArray:clientIds];
    [query whereKey:AVIMAttr(@"type") equalTo:[NSNumber numberWithInt:0]];
[query findConversationsWithCallback:^(NSArray *objects, NSError *error) {
    
    NSLog(@"objects=%@",objects);
    
    if (error) {
        NSLog(@"error=%@",error);
        
    }
    else if (!objects || [objects count] < 1) {
        
        //没有历史对话，就创建新的对话
        [myClient createConversationWithName:nil clientIds:clientIds attributes:@{@"type":[NSNumber numberWithInt:0]} options:AVIMConversationOptionNone callback:^(AVIMConversation *conversation, NSError *error) {
            if (error) {
                 NSLog(@"error=%@",error);
            } else {
                [self openConversation:conversation];//打开对话
            }
        }];
        
        
    }
    else {
        //存在历史对话-则打开对话
        AVIMConversation *conversation = [objects objectAtIndex:0];
        [self openConversation:conversation];
    }
    
    
    
}];
    
    NSLog(@"选择usernem=%@",chatToUser.username);
     [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)openConversation:(AVIMConversation*)conversation {
    TalkViewController *tVC = [[TalkViewController alloc] init];
    tVC.conversation = conversation;
    
   // AVUser *chatToUser = self.listArray[indexPath.row];
    tVC.toWho =self.toWho;
    [self.navigationController pushViewController:tVC animated:YES];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"解除好友关系吗" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        alertView.tag = indexPath.row;
        [alertView show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"点击删除好友");
        //删除好友
        //删除两人的对话
        //删除我对对方的关注
        //删除对方对我的关注
        
        [self showProgress];
        AVUser *myUser=[AVUser currentUser];
        AVUser *deleteUser=self.listArray[alertView.tag];
        
        [myUser unfollow:deleteUser.objectId andCallback:^(BOOL succeeded, NSError *error) {
            
            
            
            if (succeeded) {
                
                NSLog(@"我取消对对方的关注");
                
                [[UserHandle shareUser] tryCreateDeleteFriendRequestWithToUser:deleteUser callback:^(BOOL succeeded, NSError *error) {}];
                
                
                
                [self hideProgress];
                
                [self getFriendList];
                
                
            }
            else { NSLog(@"我取消对对方关注关注出错error=%@",error);}
            
            
        }];
        
       
        
       }
  
}
@end
