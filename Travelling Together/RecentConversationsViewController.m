//
//  RecentConversationsViewController.m
//  Travelling Together
//
//  Created by 李宁 on 15/12/5.
//  Copyright © 2015年 lanou3g. All rights reserved.
//
#import "ConversationCell.h"
#import "TalkViewController.h"
#import "myConversations.h"
//#import "ChatTableViewCell.h"
#import "RecentConversationsViewController.h"
#import "UserHandle.h"
@interface RecentConversationsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableview;
@property(nonatomic,strong)NSMutableArray *recentConsArray;//最近的聊天
@end

@implementation RecentConversationsViewController
-(void)showProgress {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(NSMutableArray*)recentConsArray{
    if (_recentConsArray==nil) {
        _recentConsArray=[NSMutableArray arrayWithCapacity:10];
    }
    return _recentConsArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[UserHandle shareUser].jump isEqualToString:@"chat"]) {
        [UserHandle shareUser].jump=nil;
    }
    
    
    self.view.backgroundColor=[UIColor whiteColor];
    self.tableview=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableview.delegate=self;
    self.tableview.dataSource=self;
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
   // [self.tableview registerClass:[ChatTableViewCell class] forCellReuseIdentifier:@"ccell"];
    
    [self.tableview registerClass:[ConversationCell class] forCellReuseIdentifier:@"ccell"];
    
    
    [self.view addSubview:self.tableview];
    
    //获取最近的联系对话
    [self showProgress];
    typeof(self) weakSelf = self;
    //自己
    AVUser *currentUser = [AVUser currentUser];
    //聊天的数组
NSArray *clientIds = [[NSArray alloc] initWithObjects:currentUser.objectId, nil];
    //AVIMClient
    AVIMClient *myClient = [[myConversations sharedMyConversations] myClient];
    NSLog(@"最近聊天-myClient=%@",myClient);
    AVIMConversationQuery *query = [myClient conversationQuery];
    query.limit = 20;
    query.skip = 0;
    [query whereKey:kAVIMKeyMember containsAllObjectsInArray:clientIds];
    //[query whereKey:AVIMAttr(@"type") equalTo:[NSNumber numberWithInt:0]];
    [query findConversationsWithCallback:^(NSArray *objects, NSError *error) {
        
        NSLog(@"获取历史会话=%@",objects);
        
        if (error) {
            NSLog(@"获取历史会话error=%@",error);
            
        }else if(objects.count>0)
        {
            
            [self.recentConsArray addObjectsFromArray:objects];
            [weakSelf.tableview reloadData];
            
        }
       
        [self hideProgress];
        
        
    }];
    
    
    
    
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //ChatTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ccell" forIndexPath:indexPath];
    
    ConversationCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ccell" forIndexPath:indexPath];
   // NSArray *arr1=[self.recentConsArray[indexPath.row] objectForKey:@"m"];
    
    AVIMConversation *con=self.recentConsArray[indexPath.row];
    NSArray *arr=con.members;
    NSLog(@"members=%@",arr);
    
    NSMutableArray *arr2=arr.mutableCopy;
    AVUser *user=[AVUser currentUser];
    [arr2 removeObject:user.objectId];
    

    AVQuery *query = [AVQuery queryWithClassName:@"_User"];
    AVUser *auser = (AVUser*)[query getObjectWithId:arr2[0]];
    //self.toWho=auser;
    cell.nameLabel.text=auser.username;
    //cell.myLabel.text=
    
    [[UserHandle shareUser] getAvatarImageOfUser:auser block:^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cell.myIMV.image=image;
        });
    }];

     return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.recentConsArray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    
    
    AVIMConversation *con=self.recentConsArray[indexPath.row];
    NSArray *arr=con.members;
    NSLog(@"members=%@",arr);
    
    NSMutableArray *arr2=arr.mutableCopy;
    AVUser *user=[AVUser currentUser];
    [arr2 removeObject:user.objectId];
    
    
    AVQuery *query = [AVQuery queryWithClassName:@"_User"];
    AVUser *auser = (AVUser*)[query getObjectWithId:arr2[0]];
    
    
    TalkViewController *tVC = [[TalkViewController alloc] init];
    tVC.conversation = con;
     tVC.toWho =auser;
    //tVC.navigationController.
    [self.navigationController pushViewController:tVC animated:YES];
   
    
    
    
    
}

@end
