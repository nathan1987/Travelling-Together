//
//  ShowCollectionViewCell.m
//  Travelling Together
//
//  Created by 段凯 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "ShowCollectionViewCell.h"
#import "UIImageView+WebCache.h"


@interface ShowCollectionViewCell ()


@end

@implementation ShowCollectionViewCell

-(void)setContentWith:(ShowModel *)model
{
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:[model.image_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"Image_no_data.png"]];
    
    
    NSLog(@"model.image_url=%@",model.image_url);
    
    self.titleLabel.text = model.name_zh_cn;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.enLabel.text = model.name_en;
    self.enLabel.textColor = [UIColor whiteColor];
  
}

- (void)awakeFromNib {
    // Initialization code
}

@end
