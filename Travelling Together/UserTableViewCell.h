//
//  UserTableViewCell.h
//  Travelling Together
//
//  Created by 李宁 on 15/12/1.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTableViewCell : UITableViewCell
@property(nonatomic,strong)UIImageView * myIMV;//头像
@property(nonatomic,strong)UILabel * nameLabel;//昵称
@property(nonatomic,strong)UILabel * emailLabel;//邮箱

@property(nonatomic,strong)UILabel * emailTestLabel;//邮箱是否验证
@end
