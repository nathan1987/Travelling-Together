//
//  IATViewController.m
//  KeDa
//
//  Created by 段凯 on 15/12/5.
//  Copyright © 2015年 段凯. All rights reserved.
//
#import "ChangeLanViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "IATViewController.h"
#import "IATConfig.h"
#import "UserHandle.h"
@interface IATViewController ()<IFlySpeechRecognizerDelegate,IFlyRecognizerViewDelegate,UIActionSheetDelegate>

@property (nonatomic, strong) NSString *pcmFilePath;//音频文件路径
@property (nonatomic,strong)IFlyRecognizerView *iflyRecognizerView;//带界面的识别对象
@property (nonatomic, strong) IFlyDataUploader *uploader;//数据上传对象
@property (nonatomic,strong)UITextView *textView;
@property (nonatomic,strong)UIButton *startButton;
@property(nonatomic,strong)UILabel *toLanguage;
@property(nonatomic,strong)UIButton *toLanButton;

@property(nonatomic,strong)UILabel *Language;



@property (nonatomic,strong)UIButton *startWordButton;

@property (nonatomic,strong)UIButton *delButton;

@end

@implementation IATViewController
-(NSString*)pcmFilePath{
    if (_pcmFilePath==nil) {
        
        //demo录音文件保存路径
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cachePath = [paths objectAtIndex:0];
        _pcmFilePath = [[NSString alloc] initWithFormat:@"%@",[cachePath stringByAppendingPathComponent:@"asr.pcm"]];
        
    }
    return _pcmFilePath;
}
-(IFlyDataUploader*)uploader{
    if (_uploader==nil) {
        _uploader=[[IFlyDataUploader alloc]init];
    }
    return _uploader;
}
-(void)showProgress {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)hideProgress {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)changelan:(UIButton*)sender{
    NSLog(@"更改目标语言");
    
    ChangeLanViewController *clVC =[[ChangeLanViewController alloc]init];
    [self presentViewController:clVC animated:YES completion:nil];
    
    
    
}
-(void)p_setupViews{
    
    self.title = @"语音助手";
    

    
    
    self.view.backgroundColor=[UIColor whiteColor];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"yuyinbeijing.png"]]];
    
    
    self.toLanguage = [[UILabel alloc]init];
    self.toLanguage . frame = CGRectMake(10, 70, 80, 30);
    self.toLanguage.text = @"翻译成:";
    
    NSInteger n=[UserHandle shareUser].defaultLan;
    NSString *lan= [UserHandle shareUser].lanArr[n];
    
    self.Language = [[UILabel alloc]init];
    self.Language . frame = CGRectMake(100, 70, 80, 30);
    self.Language.text =lan;
    
    
    
    self.toLanButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.toLanButton.frame = CGRectMake(190, 70, 80, 30);
   // self.toLanButton.titleLabel.text=lan;
   // self.toLanButton.titleLabel.tintColor=[UIColor greenColor];
    [self.toLanButton setTitle:@"选择" forState:UIControlStateNormal];
    [self.view addSubview:self.toLanguage];
    [self.view addSubview:self.toLanButton];
    [self.view addSubview:self.Language];
    [self.toLanButton addTarget:self action:@selector(changelan:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 120, 356, 200)];
    self.textView.backgroundColor = [UIColor colorWithRed:0.9 green:0.5 blue:0.1 alpha:0.1];

    
    self.textView.layer.cornerRadius =10.0;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.textView.font = [UIFont systemFontOfSize:21.0f];
    [self.view addSubview:self.textView];
    
    self.startButton = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.textView.frame), CGRectGetMaxY(self.textView.frame) + 20, self.textView.frame.size.width - 100, self.textView.frame.size.height - 170)];
    //self.startButton.titleLabel.text = @"点击";
    [self.startButton setTitle:@"开始语音翻译" forState:UIControlStateNormal];
//    self.startButton.backgroundColor = [UIColor blueColor];
    [self.startButton addTarget:self action:@selector(buttonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.startButton];
    
    
    self.startWordButton = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.textView.frame), CGRectGetMaxY(self.startButton.frame) + 20, self.textView.frame.size.width - 100, self.textView.frame.size.height - 170)];
    //self.startButton.titleLabel.text = @"点击";
    [self.startWordButton setTitle:@"开始文字翻译" forState:UIControlStateNormal];
//    self.startWordButton.backgroundColor = [UIColor blueColor];
    [self.startWordButton addTarget:self action:@selector(buttonwordAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.startWordButton];
    
    
    self.delButton = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.textView.frame), CGRectGetMaxY(self.startWordButton.frame) + 20, self.textView.frame.size.width - 100, self.textView.frame.size.height - 170)];
    //self.startButton.titleLabel.text = @"点击";
    [self.delButton setTitle:@"清空文字" forState:UIControlStateNormal];
//    self.delButton.backgroundColor = [UIColor blueColor];
    [self.delButton addTarget:self action:@selector(delAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.delButton];
    
}
-(void)delAction:(UIButton*)sender{
    NSLog(@"清空文字");
    self.textView.text=@"";
}
-(void)buttonwordAction:(UIButton*)sender{
    NSLog(@"开始文字翻译");
    
    
    
    //appid
    NSString *appid=@"20151216000007858";
    //秘钥
    NSString *key=@"zot9SXuJmO7Kh5GL4f0y";
    //要翻译的字符串
    NSString *q=self.textView.text;
    // const char *qq =[q UTF8String];
    NSString *dataUTF8 = [q stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSString *qq = [NSString stringWithCString:@"nice to meet you" encoding:NSUTF8StringEncoding];
    //随机数
    NSString *salt=@"1435660208";
    //拼接字符串
    NSString *str1 = [NSString stringWithFormat:@"%@%@%@%@",appid,q,salt,key];
    NSString *sign=[self createMD5:str1];
    NSInteger m=[UserHandle shareUser].defaultLan;
    NSString *language=[UserHandle shareUser].lanCodeArr[m];
    
    NSString *s2=[NSString stringWithFormat:@"http://api.fanyi.baidu.com/api/trans/vip/translate?q=%@&from=auto&to=%@&appid=20151216000007858&salt=1435660208&sign=%@",q,language,sign];
    
    NSString* s4 = [s2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *s3=@"http://api.fanyi.baidu.com/api/trans/vip/translate?q=apple&from=en&to=zh&appid=2015063000000001&salt=1435660288&sign=f89f9594663708c1605f3d736d01d2d4";
    //F89F9594663708C1605F3D736D01D2D4
    
    // NSLog(@"s4=%@",s4);
    
    
    NSURL *url=[NSURL URLWithString:s4];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    
    NSURLSession * session=[NSURLSession sharedSession];
    
    NSURLSessionDataTask *task= [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"翻译返回");
        if (data) {
            
            NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"数据%@",dic);
            NSString *sss=[dic[@"trans_result"][0] valueForKey:@"dst"];
            NSLog(@"ss=%@",sss);
            NSString *sss1=_textView.text;
            NSString *sss3=[NSString stringWithFormat:@"%@\n翻译后：%@",sss1,sss];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _textView.text =sss3;
            });
        }else{
            NSLog(@"error=%@",error);
        }
        
        //[self hideProgress];
    }];
    
    [task resume];//恢复
    
    
    
    
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSInteger n=[UserHandle shareUser].defaultLan;
    NSString *lan= [UserHandle shareUser].lanArr[n];
    
   
    self.Language.text =lan;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self p_setupViews];
    // Do any additional setup after loading the view.
}

//离开这个页面
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_iflyRecognizerView cancel]; //取消识别
    [_iflyRecognizerView setDelegate:nil];
    [_iflyRecognizerView setParameter:@"" forKey:[IFlySpeechConstant PARAMS]];
    
}


//点击开始语音识别
- (void)buttonAction:(UIButton *)sender
{
    NSLog(@"开始语义识别");
    
    if(_iflyRecognizerView == nil)
    {
        [self initRecognizer ];
    }
    
    if(_iflyRecognizerView.delegate==nil) _iflyRecognizerView.delegate=self;
    
    NSLog(@"_iflyRecognizerView=%@",_iflyRecognizerView);
    
    [_textView setText:@""];
    [_textView resignFirstResponder];
    
    //设置音频来源为麦克风
    [_iflyRecognizerView setParameter:IFLY_AUDIO_SOURCE_MIC forKey:@"audio_source"];
    
    //设置听写结果格式为json
    [_iflyRecognizerView setParameter:@"plain" forKey:[IFlySpeechConstant RESULT_TYPE]];
    
    //保存录音文件，保存在sdk工作路径中，如未设置工作路径，则默认保存在library/cache下
    [_iflyRecognizerView setParameter:@"asr.pcm" forKey:[IFlySpeechConstant ASR_AUDIO_PATH]];
    
   BOOL start = [_iflyRecognizerView start];
    NSLog(@"开始：%d",start);
    
}


- (void)initRecognizer
{
    //单例模式，UI的实例
    if (_iflyRecognizerView == nil) {
        //UI显示剧中
        _iflyRecognizerView= [[IFlyRecognizerView alloc] initWithCenter:self.view.center];
        
        [_iflyRecognizerView setParameter:@"" forKey:[IFlySpeechConstant PARAMS]];
        
        //设置听写模式
        [_iflyRecognizerView setParameter:@"iat" forKey:[IFlySpeechConstant IFLY_DOMAIN]];
        
    }
    _iflyRecognizerView.delegate = self;
    
    if (_iflyRecognizerView != nil) {
        IATConfig *instance = [IATConfig sharedInstance];
        //设置最长录音时间
        [_iflyRecognizerView setParameter:instance.speechTimeout forKey:[IFlySpeechConstant SPEECH_TIMEOUT]];
        //设置后端点
        [_iflyRecognizerView setParameter:instance.vadEos forKey:[IFlySpeechConstant VAD_EOS]];
        //设置前端点
        [_iflyRecognizerView setParameter:instance.vadBos forKey:[IFlySpeechConstant VAD_BOS]];
        //网络等待时间
        [_iflyRecognizerView setParameter:@"20000" forKey:[IFlySpeechConstant NET_TIMEOUT]];
        
        //设置采样率，推荐使用16K
        [_iflyRecognizerView setParameter:instance.sampleRate forKey:[IFlySpeechConstant SAMPLE_RATE]];
        if ([instance.language isEqualToString:[IATConfig chinese]]) {
            //设置语言
            [_iflyRecognizerView setParameter:instance.language forKey:[IFlySpeechConstant LANGUAGE]];
            //设置方言
            [_iflyRecognizerView setParameter:instance.accent forKey:[IFlySpeechConstant ACCENT]];
        }else if ([instance.language isEqualToString:[IATConfig english]]) {
            //设置语言
            [_iflyRecognizerView setParameter:instance.language forKey:[IFlySpeechConstant LANGUAGE]];
        }
        //设置是否返回标点符号
        [_iflyRecognizerView setParameter:instance.dot forKey:[IFlySpeechConstant ASR_PTT]];
        
    }
}

/**
 有界面，听写结果回调
 resultArray：听写结果
 isLast：表示最后一次
 ****/
- (void)onResult:(NSArray *)resultArray isLast:(BOOL)isLast
{
    NSMutableString *result = [[NSMutableString alloc] init];
    NSDictionary *dic = [resultArray objectAtIndex:0];
    for (NSString *key in dic) {
        [result appendFormat:@"%@",key];
    }

    
    NSLog(@"resultArray=%@,result=%@",resultArray,result);
    _textView.text = [NSString stringWithFormat:@"%@%@",_textView.text,result];

    _textView.text = [NSString stringWithFormat:@"%@",result];
    
    NSLog(@"result=%@",result);
    NSLog(@"dic=%@",dic);
    
    //[self showProgress];//开始将字符串发送到百度
    
    //appid
    NSString *appid=@"20151216000007858";
    //秘钥
    NSString *key=@"zot9SXuJmO7Kh5GL4f0y";
    //要翻译的字符串
    NSString *q=result;
   // const char *qq =[q UTF8String];
        NSString *dataUTF8 = [q stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   // NSString *qq = [NSString stringWithCString:@"nice to meet you" encoding:NSUTF8StringEncoding];
    //随机数
    NSString *salt=@"1435660208";
    //拼接字符串
    NSString *str1 = [NSString stringWithFormat:@"%@%@%@%@",appid,q,salt,key];
    NSString *sign=[self createMD5:str1];
    NSInteger m=[UserHandle shareUser].defaultLan;
    NSString *language=[UserHandle shareUser].lanCodeArr[m];
    
    NSString *s2=[NSString stringWithFormat:@"http://api.fanyi.baidu.com/api/trans/vip/translate?q=%@&from=auto&to=%@&appid=20151216000007858&salt=1435660208&sign=%@",q,language,sign];
    
    NSString* s4 = [s2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *s3=@"http://api.fanyi.baidu.com/api/trans/vip/translate?q=apple&from=en&to=zh&appid=2015063000000001&salt=1435660288&sign=f89f9594663708c1605f3d736d01d2d4";
    //F89F9594663708C1605F3D736D01D2D4
    
   // NSLog(@"s4=%@",s4);
    
    
    NSURL *url=[NSURL URLWithString:s4];
    
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    
        NSURLSession * session=[NSURLSession sharedSession];
    
    NSURLSessionDataTask *task= [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"翻译返回");
        if (data) {
            
        NSDictionary *dic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        NSLog(@"数据%@",dic);
            NSString *sss=[dic[@"trans_result"][0] valueForKey:@"dst"];
            NSLog(@"ss=%@",sss);
            NSString *sss1=_textView.text;
            NSString *sss3=[NSString stringWithFormat:@"%@\n翻译后：%@",sss1,sss];
          
            dispatch_async(dispatch_get_main_queue(), ^{
                _textView.text =sss3;
            });
        }else{
            NSLog(@"error=%@",error);
        }
        
        //[self hideProgress];
        }];
    
    [task resume];//恢复
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) onError:(IFlySpeechError *) errorCode{
    NSLog(@"errorCode=%@",errorCode);
}

- (void) onResults:(NSArray *) results isLast:(BOOL)isLast{
    NSLog(@"isLast=%d",isLast);
    NSLog(@"results=%@",results);
}
//加密成md5
-(NSString *)createMD5:(NSString *)signString
{
    NSLog(@"md5加密");
    const char*cStr =[signString UTF8String];
    unsigned char result[16];
    unsigned int n=strlen(cStr);
    CC_MD5(cStr, n, result);
    NSString *s=[NSString stringWithFormat:
           @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
           result[0], result[1], result[2], result[3],
           result[4], result[5], result[6], result[7],
           result[8], result[9], result[10], result[11],
           result[12], result[13], result[14], result[15]
           ];
    NSLog(@"加密后s=%@",s);
    
    //大写%02X，小写%02x
    return s;
}
@end
