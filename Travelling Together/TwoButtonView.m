//
//  TwoButtonView.m
//  TwoTableView
//
//  Created by 苗旭萌 on 15/11/11.
//  Copyright © 2015年 苗旭萌. All rights reserved.
//

#import "TwoButtonView.h"

@interface TwoButtonView()

@property (nonatomic, strong) UIButton *buttonOne;

@property (nonatomic, strong) UIButton *buttonTwo;

@property (nonatomic, strong) UIButton *saveButton;

@property (nonatomic, strong) UIView *redView;

@end

@implementation TwoButtonView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.buttonOne = [UIButton buttonWithType:UIButtonTypeCustom];
        
        self.buttonOne.frame = CGRectMake(0, 0, self.frame.size.width / 2, self.frame.size.height - 1);
        
        self.buttonOne.tag = 10000;
        
        [self.buttonOne addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.buttonOne setTitle:@"游记" forState:UIControlStateNormal];
        [self.buttonOne setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        
        self.buttonOne.selected = YES;
        self.buttonOne.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.buttonOne];
        
        
        self.buttonTwo = [UIButton buttonWithType:UIButtonTypeCustom];
        
        self.buttonTwo.frame = CGRectMake(self.frame.size.width / 2, 0, self.frame.size.width / 2, self.frame.size.height - 1);

        
        self.saveButton = self.buttonOne;
        
        [self.buttonTwo addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.buttonTwo setTitle:@"专题" forState: UIControlStateNormal];
        [self.buttonTwo setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        self.buttonTwo.tag = 10001;
        [self addSubview:self.buttonTwo];
        
        self.buttonTwo.backgroundColor = [UIColor whiteColor];
        
        self.redView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 1, self.frame.size.width / 2, 1)];
        self.redView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:self.redView];
        
    }
    return self;
}

- (void)buttonClicked:(UIButton *)sender
{
    
 
    if (self.saveButton != sender) {
        sender.selected = !sender.selected;
        self.saveButton.selected = !self.saveButton.selected;
        self.saveButton = sender;
        
        [UIView animateWithDuration:0.1 animations:^{
            self.redView.frame = CGRectMake(sender.frame.origin.x, self.frame.size.height - 1, self.frame.size.width / 2, 1);
            

        }];
          self.myBlock(sender.tag - 10000);
    }
}

- (void)setButtonClicked:(NSInteger)index
{
    UIButton *button = (UIButton *)[self viewWithTag:index + 10000];
    
    [self buttonClicked:button];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
